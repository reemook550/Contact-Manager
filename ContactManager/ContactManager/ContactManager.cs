﻿using ContactManager.ContactManagerClasses;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace ContactManager
{
    public partial class ContactManager : Form
    {

        //Fields
        private int borderSize = 2;
        public ContactManager() // call the funktion select 
        {
            InitializeComponent();
            c.Select();
            this.Padding = new Padding(borderSize);
            this.BackColor = Color.FromArgb(98, 102, 244);
        }

        //Drag the form 
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();

        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int Iparam);



        ContactClass c = new ContactClass(); //Create an Opject 

        private void btnAdd_Click(object sender, EventArgs e) //Add to the Datebase
        {

            // get the Values from the Input Fields  FirstName, LastName, PhoneNumber, Gender, PLZ, city, Street, StreetNumber
            Info date = new Info(textBoxContactId.Text.Trim(), TextBoxFirstName.Text.Trim(), textBoxLastName.Text.Trim(), textBoxPhoneNo.Text.Trim(), textBoxEmail.Text.Trim(), combGender.Text.Trim(),
                textBoxPlz.Text.Trim(), textBoxStreet.Text.Trim(), textBoxStreetNumber.Text.Trim(), textBoxCity.Text.Trim());

            // Inserting data into Database using the methode in ContactClass
            ContactClass.Insert(date);
            //clear after insert 
            clear();
            //Load Data on the DataGrid 
            DataTable dt = c.Select();
            dgvContactList.DataSource = dt;

        }

        private void ContactManager_Load(object sender, EventArgs e)
        {
            //Load Data on the DataGrid 
            DataTable dt = c.Select();
            dgvContactList.DataSource = dt;

        }

        private void pictureBox2_Click(object sender, EventArgs e) //Close the Application
        {
            this.Close();

        }
        public void clear()
        {
            textBoxContactId.Text = "";
            TextBoxFirstName.Text = "";
            textBoxLastName.Text = "";
            textBoxPhoneNo.Text = "";
            textBoxEmail.Text = "";
            combGender.Text = "";
            textBoxStreet.Text = "";
            textBoxStreetNumber.Text = "";
            textBoxPlz.Text = "";
            textBoxCity.Text = "";

        }//Clear all fields 

        private void panel1_MouseDown(object sender, MouseEventArgs e) //dragBar for Usabiltiy
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        private void btnUpdate_Click(object sender, EventArgs e) //Update Funktion 
        {
            // Get the Data from InputBoxes
            Info date = new Info(textBoxContactId.Text.Trim(), TextBoxFirstName.Text.Trim(), textBoxLastName.Text.Trim(), textBoxPhoneNo.Text.Trim(), textBoxEmail.Text.Trim(), combGender.Text.Trim(),
               textBoxPlz.Text.Trim(), textBoxStreet.Text.Trim(), textBoxStreetNumber.Text.Trim(), textBoxCity.Text.Trim());
            ContactClass.Update(date);
            clear();
            DataTable dt = c.Select();
            dgvContactList.DataSource = dt;
        }

        private void dgvContactList_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e) // Get The data from the GridView
        {
            // Get Ths data from gridView into input Boxes
            // Identify the row that the mouse cliked

            int rowIndex = e.RowIndex;
            textBoxContactId.Text = dgvContactList.Rows[rowIndex].Cells[0].Value.ToString();
            TextBoxFirstName.Text = dgvContactList.Rows[rowIndex].Cells[1].Value.ToString();
            textBoxLastName.Text = dgvContactList.Rows[rowIndex].Cells[2].Value.ToString();
            textBoxPhoneNo.Text = dgvContactList.Rows[rowIndex].Cells[3].Value.ToString();
            combGender.Text = dgvContactList.Rows[rowIndex].Cells[4].Value.ToString();
            textBoxEmail.Text = dgvContactList.Rows[rowIndex].Cells[5].Value.ToString();
            textBoxPlz.Text = dgvContactList.Rows[rowIndex].Cells[6].Value.ToString();
            textBoxCity.Text = dgvContactList.Rows[rowIndex].Cells[7].Value.ToString();
            textBoxStreet.Text = dgvContactList.Rows[rowIndex].Cells[8].Value.ToString();
            textBoxStreetNumber.Text = dgvContactList.Rows[rowIndex].Cells[9].Value.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            this.clear();
        }



        private void dataGridView1_RowHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)// Get The data from the GridView
        {  // Get Ths data from gridView into input Boxes
            // Identify the row that the mouse cliked

            int rowIndex = e.RowIndex;
            textBoxContactId.Text = dgvContactList.Rows[rowIndex].Cells[0].Value.ToString();
            TextBoxFirstName.Text = dgvContactList.Rows[rowIndex].Cells[1].Value.ToString();
            textBoxLastName.Text = dgvContactList.Rows[rowIndex].Cells[2].Value.ToString();
            textBoxPhoneNo.Text = dgvContactList.Rows[rowIndex].Cells[3].Value.ToString();
            combGender.Text = dgvContactList.Rows[rowIndex].Cells[4].Value.ToString();
            textBoxEmail.Text = dgvContactList.Rows[rowIndex].Cells[5].Value.ToString();
            textBoxPlz.Text = dgvContactList.Rows[rowIndex].Cells[6].Value.ToString();
            textBoxCity.Text = dgvContactList.Rows[rowIndex].Cells[7].Value.ToString();
            textBoxStreet.Text = dgvContactList.Rows[rowIndex].Cells[8].Value.ToString();
            textBoxStreetNumber.Text = dgvContactList.Rows[rowIndex].Cells[9].Value.ToString();

        }

        private void btnDelete_Click(object sender, EventArgs e) // delete 
        {

            Info date = new Info(textBoxContactId.Text.Trim(), TextBoxFirstName.Text.Trim(), textBoxLastName.Text.Trim(), textBoxPhoneNo.Text.Trim(), textBoxEmail.Text.Trim(), combGender.Text.Trim(),
              textBoxPlz.Text.Trim(), textBoxStreet.Text.Trim(), textBoxStreetNumber.Text.Trim(), textBoxCity.Text.Trim());
            ContactClass.Delete(date);
            clear();
            DataTable dt = c.Select();
            dgvContactList.DataSource = dt;

        }

        private void dgvContactList_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            DataGridView gridView = sender as DataGridView;
            if (null != gridView)
            {
                foreach (DataGridViewRow r in gridView.Rows)
                {
                    gridView.Rows[r.Index].HeaderCell.Value =
                                        (r.Index + 1).ToString();
                }
            }
        }



        private void button1_Click(object sender, EventArgs e)
        {

            string filename = "Export " + DateTime.Now.ToString("yyyy_MM_dd");
            string strpath = (@"R:\" + filename + ".vcf");
            System.IO.StreamWriter file = new System.IO.StreamWriter(strpath);
            try
            {
                var vcf = new StringBuilder();

                //This for loop loops through each row in the table
                for (int r = 0; r <= dgvContactList.SelectedRows.Count - 1; r++)
                {
                    //This for loop loops through each column, and the row number
                    //is passed from the for loop above.
              
                        if (dgvContactList.SelectedRows.Count != 0)
                        {
                            DataGridViewRow row = this.dgvContactList.SelectedRows[0];
                            vcf.Append("BEGIN:VCARD" + System.Environment.NewLine);
                            vcf.Append("VERSION:3.0" + System.Environment.NewLine);
                            vcf.Append("N;" + row.Cells["FirstName"].Value + System.Environment.NewLine);
                            vcf.Append("N;" + row.Cells["LastName"].Value + System.Environment.NewLine);
                            vcf.Append("TEL;" + row.Cells["PhoneNumber"].Value + System.Environment.NewLine);
                            vcf.Append("GENDER;" + row.Cells["Gender"].Value + System.Environment.NewLine);
                            vcf.Append("EMAIL;" + row.Cells["Mail"].Value + System.Environment.NewLine);
                            vcf.Append("END:VCARD\n" + System.Environment.NewLine);
                        }

                    file.WriteLine(vcf);

                }

                file.Close();
                System.Windows.Forms.MessageBox.Show("Export Complete.", "Program Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (System.Exception err)
            {
                System.Windows.Forms.MessageBox.Show(err.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                file.Close();
            }


        }


    }
}
