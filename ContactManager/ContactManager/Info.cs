﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContactManager
{
    class Info
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string Mail { get; set; }
        public string Gender { get; set; }
        public string Plz { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public string City { get; set; }

        public Info(string id,string fnName, string lName, string phone,string mail, string gender, string plz, string street, string streetNummber, string city)
        {
            Id = id;
            FirstName = fnName;
            LastName = lName;
            PhoneNumber = phone;
            Mail = mail;
            Gender = gender;
            Plz = plz;
            Street = street;
            StreetNumber = streetNummber;
            City = city;

        }

    }
}
