﻿using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace ContactManager.ContactManagerClasses
{
    class ContactClass
    {

        string sqlc = "datasource = localhost; port = 3306; username = root; password =; database=contactmanager";
        public static MySqlConnection GetConnection()        ///Eine Verbindung zum Datenbank herstellen.
        {
            string sql = "datasource = localhost; port = 3306; username = root; password =; database=contactmanager";
            MySqlConnection con = new MySqlConnection(sql);

            try            //Mit Try und Catch wird hier Fehler, also Exceptions (Ausnahmen) abgefangen und behandelt
            {
                con.Open();
            }
            catch (MySqlException ex) // Fehler Meldung Falls keine Verbindung ist möglich
            {
                MessageBox.Show("MySQL Connection! \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            return con;
        }
        public DataTable Select() // Selecting Data from datebase
        {

            // database Connection
            MySqlConnection conn = new MySqlConnection(sqlc);
            DataTable dt = new DataTable();
            try
            {

                // Writing the Sql quiry to select data from Database 
                string sql = "SELECT * FROM contacts";
                //Creating cmd using Sql Command 
                MySqlCommand cmd = new MySqlCommand(sql, conn);
                // Creating SQL DataAdabter using cmd
                MySqlDataAdapter adapter = new MySqlDataAdapter(cmd);
                //Open The Connection
                conn.Open();
                //fill the datatabell 
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Can not Select Data. \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Close the connection 
                conn.Close();
            }
            //return our datatabel 
            return dt;
        }
        public static void Insert(Info c) // Inserting Data into our Database 
        {

            //Creating the default return type and setting the value to false 

            bool isSuccess = false;
            //Connect the database
            MySqlConnection conn = GetConnection();

            //Create Sql quiry to insert Data 
            string sql = "INSERT INTO contacts (FirstName, LastName, PhoneNumber,Gender, Mail,PLZ,City, Street, StreetNumber) VALUES (@FirstName, @LastName, @PhoneNumber,@Gender, @Mail,@PLZ,@City, @Street, @StreetNumber)";

            //Creating the Sql command using SQL and conn
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            cmd.CommandType = CommandType.Text;

            //ceate Parameters to add data into our database
            cmd.Parameters.Add("@FirstName", MySqlDbType.VarChar).Value = c.FirstName;
            cmd.Parameters.Add("@LastName", MySqlDbType.VarChar).Value = c.LastName;
            cmd.Parameters.Add("@PhoneNumber", MySqlDbType.VarChar).Value = c.PhoneNumber;
            cmd.Parameters.Add("@Gender", MySqlDbType.VarChar).Value = c.Gender;
            cmd.Parameters.Add("@Mail", MySqlDbType.VarChar).Value = c.Mail;
            cmd.Parameters.Add("@PLZ", MySqlDbType.VarChar).Value = c.Plz;
            cmd.Parameters.Add("@City", MySqlDbType.VarChar).Value = c.City;
            cmd.Parameters.Add("@Street", MySqlDbType.VarChar).Value = c.Street;
            cmd.Parameters.Add("@StreetNumber", MySqlDbType.VarChar).Value = c.StreetNumber;

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("new Contact Added Seccessfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (MySqlException ex)
            {
                MessageBox.Show("Contact not insert. \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }

        }
        public static void Update(Info c) // Methode to Update Data in Database from our Applicatio
        {
            //Connect the database
            MySqlConnection con = GetConnection();
            string sql = "UPDATE contacts SET FirstName=@FirstName, LastName=@LastName,PhoneNumber=@PhoneNumber, Gender=@Gender, PLZ=@PLZ, City=@City, Mail=@Mail, Street=@Street, StreetNumber=@StreetNumber WHERE ContactID=@ContactID";
            //Create Sql quiry to Update Data in our Database
            // Creating the Sql command using SQL and conn
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;

            //ceate Parameters to add data into our database
            cmd.Parameters.Add("@ContactID", MySqlDbType.Int32).Value = c.Id;
            cmd.Parameters.Add("@FirstName", MySqlDbType.VarChar).Value = c.FirstName;
            cmd.Parameters.Add("@LastName", MySqlDbType.VarChar).Value = c.LastName;
            cmd.Parameters.Add("@PhoneNumber", MySqlDbType.VarChar).Value = c.PhoneNumber;
            cmd.Parameters.Add("@Gender", MySqlDbType.VarChar).Value = c.Gender;
            cmd.Parameters.Add("@Mail", MySqlDbType.VarChar).Value = c.Mail;
            cmd.Parameters.Add("@PLZ", MySqlDbType.VarChar).Value = c.Plz;
            cmd.Parameters.Add("@City", MySqlDbType.VarChar).Value = c.City;
            cmd.Parameters.Add("@Street", MySqlDbType.VarChar).Value = c.Street;
            cmd.Parameters.Add("@StreetNumber", MySqlDbType.VarChar).Value = c.StreetNumber;

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contact Updated Seccessfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            catch (MySqlException ex)
            {
                MessageBox.Show("Contact not Update. \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {

                //Close the connection 
                con.Close();
            }
        }
        public static void Delete(Info c)   // Methode to delete Data in Database from our Applicatio
        {

            //Connect the database
            MySqlConnection con = GetConnection();
            // Step2: Create Sql quiry to Update Data in our Database
            string sql = "DELETE FROM Contacts WHERE ContactID= @ContactID";
            // Creating the Sql command using SQL and conn
            MySqlCommand cmd = new MySqlCommand(sql, con);
            cmd.CommandType = CommandType.Text;

            //ceate Parameters to add data into our database
            cmd.Parameters.Add("@ContactID", MySqlDbType.VarChar).Value = c.Id;

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Contact deleted Seccessfully.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Contact not deleted. \n" + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                //Close the connection 
                con.Close();
            }


        }


    }
}
