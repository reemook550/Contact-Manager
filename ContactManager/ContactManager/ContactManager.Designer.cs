﻿
using System;

namespace ContactManager
{
    partial class ContactManager
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContactManager));
            this.ContactID = new System.Windows.Forms.Label();
            this.textBoxContactId = new System.Windows.Forms.TextBox();
            this.TextBoxFirstName = new System.Windows.Forms.TextBox();
            this.FirstName = new System.Windows.Forms.Label();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.LastName = new System.Windows.Forms.Label();
            this.textBoxPhoneNo = new System.Windows.Forms.TextBox();
            this.ContactNo = new System.Windows.Forms.Label();
            this.textBoxEmail = new System.Windows.Forms.TextBox();
            this.Email = new System.Windows.Forms.Label();
            this.Gender = new System.Windows.Forms.Label();
            this.combGender = new System.Windows.Forms.ComboBox();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBoxPlz = new System.Windows.Forms.TextBox();
            this.labelPlz = new System.Windows.Forms.Label();
            this.textBoxCity = new System.Windows.Forms.TextBox();
            this.labelCity = new System.Windows.Forms.Label();
            this.textBoxStreet = new System.Windows.Forms.TextBox();
            this.labelStreet = new System.Windows.Forms.Label();
            this.textBoxStreetNumber = new System.Windows.Forms.TextBox();
            this.labelStreetNumber = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvContactList = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContactList)).BeginInit();
            this.SuspendLayout();
            // 
            // ContactID
            // 
            this.ContactID.AutoSize = true;
            this.ContactID.BackColor = System.Drawing.Color.Transparent;
            this.ContactID.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ContactID.ForeColor = System.Drawing.Color.White;
            this.ContactID.Location = new System.Drawing.Point(43, 173);
            this.ContactID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ContactID.Name = "ContactID";
            this.ContactID.Size = new System.Drawing.Size(134, 28);
            this.ContactID.TabIndex = 1;
            this.ContactID.Text = "Contact ID";

            // 
            // textBoxContactId
            // 
            this.textBoxContactId.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxContactId.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxContactId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxContactId.Location = new System.Drawing.Point(225, 174);
            this.textBoxContactId.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxContactId.Name = "textBoxContactId";
            this.textBoxContactId.ReadOnly = true;
            this.textBoxContactId.Size = new System.Drawing.Size(225, 23);
            this.textBoxContactId.TabIndex = 2;

            // 
            // TextBoxFirstName
            // 
            this.TextBoxFirstName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBoxFirstName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.TextBoxFirstName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.TextBoxFirstName.Location = new System.Drawing.Point(225, 223);
            this.TextBoxFirstName.Margin = new System.Windows.Forms.Padding(0);
            this.TextBoxFirstName.Name = "TextBoxFirstName";
            this.TextBoxFirstName.PlaceholderText = "write your first name";
            this.TextBoxFirstName.Size = new System.Drawing.Size(225, 23);
            this.TextBoxFirstName.TabIndex = 4;

            // 
            // FirstName
            // 
            this.FirstName.AutoSize = true;
            this.FirstName.BackColor = System.Drawing.Color.Transparent;
            this.FirstName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.FirstName.ForeColor = System.Drawing.Color.White;
            this.FirstName.Location = new System.Drawing.Point(43, 222);
            this.FirstName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.FirstName.Name = "FirstName";
            this.FirstName.Size = new System.Drawing.Size(138, 28);
            this.FirstName.TabIndex = 3;
            this.FirstName.Text = "First Name";

            // 
            // textBoxLastName
            // 
            this.textBoxLastName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxLastName.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxLastName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxLastName.Location = new System.Drawing.Point(225, 269);
            this.textBoxLastName.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.PlaceholderText = "write your Last name";
            this.textBoxLastName.Size = new System.Drawing.Size(225, 23);
            this.textBoxLastName.TabIndex = 6;
         
            // 
            // LastName
            // 
            this.LastName.AutoSize = true;
            this.LastName.BackColor = System.Drawing.Color.Transparent;
            this.LastName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.LastName.ForeColor = System.Drawing.Color.White;
            this.LastName.Location = new System.Drawing.Point(43, 263);
            this.LastName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LastName.Name = "LastName";
            this.LastName.Size = new System.Drawing.Size(134, 28);
            this.LastName.TabIndex = 5;
            this.LastName.Text = "Last Name";
         
            // 
            // textBoxPhoneNo
            // 
            this.textBoxPhoneNo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPhoneNo.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxPhoneNo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxPhoneNo.Location = new System.Drawing.Point(224, 309);
            this.textBoxPhoneNo.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxPhoneNo.Name = "textBoxPhoneNo";
            this.textBoxPhoneNo.PlaceholderText = "Insert the Phone Number";
            this.textBoxPhoneNo.Size = new System.Drawing.Size(225, 23);
            this.textBoxPhoneNo.TabIndex = 8;
    
            // 
            // ContactNo
            // 
            this.ContactNo.AutoSize = true;
            this.ContactNo.BackColor = System.Drawing.Color.Transparent;
            this.ContactNo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ContactNo.ForeColor = System.Drawing.Color.White;
            this.ContactNo.Location = new System.Drawing.Point(43, 309);
            this.ContactNo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.ContactNo.Name = "ContactNo";
            this.ContactNo.Size = new System.Drawing.Size(183, 28);
            this.ContactNo.TabIndex = 7;
            this.ContactNo.Text = "Phone Number";
     
            // 
            // textBoxEmail
            // 
            this.textBoxEmail.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxEmail.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxEmail.Location = new System.Drawing.Point(843, 185);
            this.textBoxEmail.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxEmail.Name = "textBoxEmail";
            this.textBoxEmail.PlaceholderText = "Insert a valid E-Mail ";
            this.textBoxEmail.Size = new System.Drawing.Size(278, 23);
            this.textBoxEmail.TabIndex = 10;
            // 
            // Email
            // 
            this.Email.AutoSize = true;
            this.Email.BackColor = System.Drawing.Color.Transparent;
            this.Email.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Email.ForeColor = System.Drawing.Color.White;
            this.Email.Location = new System.Drawing.Point(659, 181);
            this.Email.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Email.Name = "Email";
            this.Email.Size = new System.Drawing.Size(84, 28);
            this.Email.TabIndex = 9;
            this.Email.Text = "E-Mail";
            // 
            // Gender
            // 
            this.Gender.AutoSize = true;
            this.Gender.BackColor = System.Drawing.Color.Transparent;
            this.Gender.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Gender.ForeColor = System.Drawing.Color.White;
            this.Gender.Location = new System.Drawing.Point(43, 346);
            this.Gender.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Gender.Name = "Gender";
            this.Gender.Size = new System.Drawing.Size(99, 28);
            this.Gender.TabIndex = 11;
            this.Gender.Text = "Gender";

            // 
            // combGender
            // 
            this.combGender.Cursor = System.Windows.Forms.Cursors.Hand;
            this.combGender.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.combGender.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.combGender.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.combGender.FormattingEnabled = true;
            this.combGender.Items.AddRange(new object[] {
            "Male",
            "Female",
            "divers"});
            this.combGender.Location = new System.Drawing.Point(225, 346);
            this.combGender.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.combGender.Name = "combGender";
            this.combGender.Size = new System.Drawing.Size(279, 31);
            this.combGender.TabIndex = 13;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.DarkSeaGreen;
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(648, 881);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(126, 45);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.LightSkyBlue;
            this.btnUpdate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpdate.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnUpdate.ForeColor = System.Drawing.Color.White;
            this.btnUpdate.Location = new System.Drawing.Point(792, 881);
            this.btnUpdate.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(126, 45);
            this.btnUpdate.TabIndex = 15;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.BackColor = System.Drawing.Color.Salmon;
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnDelete.ForeColor = System.Drawing.Color.White;
            this.btnDelete.Location = new System.Drawing.Point(937, 881);
            this.btnDelete.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(126, 45);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.Text = "Delete";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.BackColor = System.Drawing.Color.SandyBrown;
            this.btnClear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClear.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnClear.ForeColor = System.Drawing.Color.White;
            this.btnClear.Location = new System.Drawing.Point(1078, 881);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(126, 45);
            this.btnClear.TabIndex = 17;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = false;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(24, 27);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(389, 112);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // textBoxPlz
            // 
            this.textBoxPlz.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPlz.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxPlz.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxPlz.Location = new System.Drawing.Point(843, 224);
            this.textBoxPlz.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxPlz.Name = "textBoxPlz";
            this.textBoxPlz.PlaceholderText = "City Post Code";
            this.textBoxPlz.Size = new System.Drawing.Size(278, 23);
            this.textBoxPlz.TabIndex = 23;
            // 
            // labelPlz
            // 
            this.labelPlz.AutoSize = true;
            this.labelPlz.BackColor = System.Drawing.Color.Transparent;
            this.labelPlz.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelPlz.ForeColor = System.Drawing.Color.White;
            this.labelPlz.Location = new System.Drawing.Point(659, 219);
            this.labelPlz.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelPlz.Name = "labelPlz";
            this.labelPlz.Size = new System.Drawing.Size(58, 28);
            this.labelPlz.TabIndex = 22;
            this.labelPlz.Text = "PLZ";
            // 
            // textBoxCity
            // 
            this.textBoxCity.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxCity.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxCity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxCity.Location = new System.Drawing.Point(843, 264);
            this.textBoxCity.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxCity.Name = "textBoxCity";
            this.textBoxCity.PlaceholderText = "Insert The City Name";
            this.textBoxCity.Size = new System.Drawing.Size(278, 23);
            this.textBoxCity.TabIndex = 25;
            // 
            // labelCity
            // 
            this.labelCity.AutoSize = true;
            this.labelCity.BackColor = System.Drawing.Color.Transparent;
            this.labelCity.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelCity.ForeColor = System.Drawing.Color.White;
            this.labelCity.Location = new System.Drawing.Point(659, 261);
            this.labelCity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelCity.Name = "labelCity";
            this.labelCity.Size = new System.Drawing.Size(58, 28);
            this.labelCity.TabIndex = 24;
            this.labelCity.Text = "City";
            // 
            // textBoxStreet
            // 
            this.textBoxStreet.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxStreet.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxStreet.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxStreet.Location = new System.Drawing.Point(843, 301);
            this.textBoxStreet.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxStreet.Name = "textBoxStreet";
            this.textBoxStreet.PlaceholderText = "Insert The Street name";
            this.textBoxStreet.Size = new System.Drawing.Size(278, 23);
            this.textBoxStreet.TabIndex = 27;
            // 
            // labelStreet
            // 
            this.labelStreet.AutoSize = true;
            this.labelStreet.BackColor = System.Drawing.Color.Transparent;
            this.labelStreet.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelStreet.ForeColor = System.Drawing.Color.White;
            this.labelStreet.Location = new System.Drawing.Point(659, 296);
            this.labelStreet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStreet.Name = "labelStreet";
            this.labelStreet.Size = new System.Drawing.Size(83, 28);
            this.labelStreet.TabIndex = 26;
            this.labelStreet.Text = "Street";
            // 
            // textBoxStreetNumber
            // 
            this.textBoxStreetNumber.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxStreetNumber.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxStreetNumber.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBoxStreetNumber.Location = new System.Drawing.Point(842, 344);
            this.textBoxStreetNumber.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.textBoxStreetNumber.Name = "textBoxStreetNumber";
            this.textBoxStreetNumber.PlaceholderText = "Insert the Street Number";
            this.textBoxStreetNumber.Size = new System.Drawing.Size(278, 23);
            this.textBoxStreetNumber.TabIndex = 29;
            // 
            // labelStreetNumber
            // 
            this.labelStreetNumber.AutoSize = true;
            this.labelStreetNumber.BackColor = System.Drawing.Color.Transparent;
            this.labelStreetNumber.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.14286F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.labelStreetNumber.ForeColor = System.Drawing.Color.White;
            this.labelStreetNumber.Location = new System.Drawing.Point(658, 339);
            this.labelStreetNumber.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStreetNumber.Name = "labelStreetNumber";
            this.labelStreetNumber.Size = new System.Drawing.Size(182, 28);
            this.labelStreetNumber.TabIndex = 28;
            this.labelStreetNumber.Text = "Street Number";
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(1136, 2);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(80, 61);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.pictureBox2.TabIndex = 30;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Location = new System.Drawing.Point(0, -1);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1132, 64);
            this.panel1.TabIndex = 31;
           
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // dgvContactList
            // 
            this.dgvContactList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvContactList.Location = new System.Drawing.Point(24, 396);
            this.dgvContactList.Margin = new System.Windows.Forms.Padding(2);
            this.dgvContactList.Name = "dgvContactList";
            this.dgvContactList.RowHeadersWidth = 62;
            this.dgvContactList.RowTemplate.Height = 33;
            this.dgvContactList.Size = new System.Drawing.Size(1169, 469);
            this.dgvContactList.TabIndex = 32;
           
            this.dgvContactList.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgvContactList_DataBindingComplete);
            this.dgvContactList.RowHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvContactList_RowHeaderMouseClick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DarkCyan;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Font = new System.Drawing.Font("Arial", 14.14286F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(842, 119);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(278, 45);
            this.button1.TabIndex = 33;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // ContactManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.MintCream;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1218, 938);
            this.ControlBox = false;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dgvContactList);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.textBoxStreetNumber);
            this.Controls.Add(this.labelStreetNumber);
            this.Controls.Add(this.textBoxStreet);
            this.Controls.Add(this.labelStreet);
            this.Controls.Add(this.textBoxCity);
            this.Controls.Add(this.labelCity);
            this.Controls.Add(this.textBoxPlz);
            this.Controls.Add(this.labelPlz);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.combGender);
            this.Controls.Add(this.Gender);
            this.Controls.Add(this.textBoxEmail);
            this.Controls.Add(this.Email);
            this.Controls.Add(this.textBoxPhoneNo);
            this.Controls.Add(this.ContactNo);
            this.Controls.Add(this.textBoxLastName);
            this.Controls.Add(this.LastName);
            this.Controls.Add(this.TextBoxFirstName);
            this.Controls.Add(this.FirstName);
            this.Controls.Add(this.textBoxContactId);
            this.Controls.Add(this.ContactID);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "ContactManager";
            this.Text = "Contact Manager";
            this.Load += new System.EventHandler(this.ContactManager_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvContactList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ContactID;
        private System.Windows.Forms.TextBox textBoxContactId;
        private System.Windows.Forms.TextBox TextBoxFirstName;
        private System.Windows.Forms.Label FirstName;
        private System.Windows.Forms.TextBox textBoxLastName;
        private System.Windows.Forms.Label LastName;
        private System.Windows.Forms.TextBox textBoxPhoneNo;
        private System.Windows.Forms.Label ContactNo;
        private System.Windows.Forms.TextBox textBoxEmail;
        private System.Windows.Forms.Label Email;
        private System.Windows.Forms.Label Gender;
        private System.Windows.Forms.ComboBox combGender;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBoxPlz;
        private System.Windows.Forms.Label labelPlz;
        private System.Windows.Forms.TextBox textBoxCity;
        private System.Windows.Forms.Label labelCity;
        private System.Windows.Forms.TextBox textBoxStreet;
        private System.Windows.Forms.Label labelStreet;
        private System.Windows.Forms.TextBox textBoxStreetNumber;
        private System.Windows.Forms.Label labelStreetNumber;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgvContactList;
        private System.Windows.Forms.Button button1;
    }
}

